const { src, dest, watch, series, parallel } = require("gulp")
const autoprefixer = require("autoprefixer"),
  concat = require("gulp-concat"),
  cssnano = require("cssnano"),
  postcss = require("gulp-postcss"),
  purge = require("gulp-css-purge"),
  rename = require("gulp-rename"),
  replace = require("gulp-replace"),
  sass = require("gulp-sass")(require("sass")),
  sourcemaps = require("gulp-sourcemaps"),
  uglify = require("gulp-uglify")
var browserSync = require("browser-sync").create()
var path = require("path")

const APPNAME = process.cwd().split(path.sep).pop()

const FILES = {
  scssWatch: [
    "./stylesheets/*.scss",
    "./stylesheets/**/*.scss",
    //'!' + 'stylesheets/a/inline.scss', // to exclude any specific files
  ],
  cssPath: [
    // './css/main.css',
    // './css/normalize.css',
    "./node_modules/sanitize.css/sanitize.css",
    // './node_modules/sanitize.css/forms.css',
    // './node_modules/sanitize.css/typography.css',
    // './node_modules/sanitize.css/page.css',
    `./css/${APPNAME}.css`,
  ],
  jsPath: [""],
  alsoWatch: ["./index.html"],
}

function scssTask() {
  return src("./stylesheets/judge.scss")
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(rename(`${APPNAME}.css`))
    .pipe(purge({ trim: false }))
    .pipe(sourcemaps.write("."))
    .pipe(dest("css"))
}

function cssTask() {
  return src(FILES.cssPath)
    .pipe(sourcemaps.init())
    .pipe(concat(`${APPNAME}.min.css`))
    .pipe(postcss([autoprefixer(), cssnano()]))
    .pipe(sourcemaps.write("."))
    .pipe(dest("dist/css"))
}

function cacheBustTask() {
  let cbString = new Date().getTime()
  return src(["index.html"])
    .pipe(replace(/cb=\d+/g, "cb=" + cbString))
    .pipe(dest("."))
}

const build = series(scssTask, cssTask)

function watchTask() {
  browserSync.init({
    server: {
      baseDir: "./",
    },
  })
  watch(FILES.scssWatch.concat(FILES.alsoWatch).concat(FILES.jsPath), build).on(
    "change",
    browserSync.reload,
  )
}

exports.build = series(build, cacheBustTask)

exports.default = series(build, watchTask)
