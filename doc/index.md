<aside>
  <dl>
  <dd>Her heavenly form</dd>
  <dd>Angelick, but more soft, and feminine,</dd>
  <dd>Her graceful innocence</dd>
</dl>
</aside>

**eve** extends **god** with some new, but consistent development _patterns_ for styling _pronged_ tagNames independently. **eve** provides a way to theme those tags when they are _believers_ or _atheists_.

With **god** taking care of the layout (see [god](/eliosin/god)), you can use the _patterns_ established in **eve** to pay attention to how those tags look.

For instance, you might put the `aside` tag in _heaven_ or _hell_ which, if you think about it, is a perfectly logically place. You might want the `aside` tag to look differently on pages in your theme when it is in _hell_ to when it is in _heaven_.

That's okay.

**eve** shows you how to style tagNames with a version ready for when they are in _pillar_, _heaven_, _hell_; or simply an _atheist_ i.e. not mentioned in the settings file at all; or just a standard inline tag like `strong` or `a`.

<div>
  <a href="demo.html" target="_demo">
  <button>Demo</button>
</a>
</div>

# Nutshell

`eves` - in principle - work in elioWays:

- An **eve** is a css class file for one tagName ...
- ... but each **eve** only affects how the tagName looks in one prong.
- tagNames are selectors, not classNames.
- Each **eve** gets its own scss file per prong so it can be imported singularly.
- `eve.scss` is referenced by `theme.scss` (or `judge.scss` if you don't want it in your theme).

In any one of **god**'s CSS styles, a tag can only be in one _prong_ or another. But with eliosin you can easily build different CSS files for different parts of the website. Treat **eve** themes like a completed sports sticker collection with a style for each tagName in every _prong_.

# A cult, not a framework

1. **eve** styles shall target tagNames not classNames.

_Terms and conditions apply. **eve** retains the right to change these rules at a whim._
