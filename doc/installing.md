# Installing eve

- [eve Prerequisites](/eliosin/eve/prerequisites.html)
- [Installing god](/eliosin/god/installing.html)

## Add it to your compile funnel

- Locate the primary `X.scss` build file within your current project stylesheets.

  - For instance, the one targeted by your compile tools in the `gruntfile`, `gulpfile` or other.

- Add the following to your current project SASS build file:

```scss
@import "../node_modules/@elioway/eve/stylesheets/settings";
@import "../node_modules/@elioway/god/stylesheets/theme";
@import "../node_modules/@elioway/eve/stylesheets/theme";
```

### Using your own settings

- Copy the `settings.scss` file from `./node_modules/@elioway/eve/stylesheets/settings.scss` -

- Paste it alongside `X.scss` into, for instance, `my_eves_settings.scss`

- Change the SASS build file making sure your settings gets called before **god**'s theme:

```scss
@import "my_eves_settings";
@import "../node_modules/@elioway/god/stylesheets/theme";
@import "../node_modules/@elioway/eve/stylesheets/theme";
```

## Nah! I'll just use the dist

```html
<link rel="stylesheet" href="node_modules/@elioway/eve/dist/css/eve.min.css" />
```

# gulp watch issues

If gulp crashes while running "watch", try this shell command in Linux.

```shell
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf
sudo sysctl -p
```
