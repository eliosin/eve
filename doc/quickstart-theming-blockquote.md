# Theme a blockquote in heaven Quickstart

We can also theme tagNames in _heaven_. Let's try a common usage and theme the `blockquote` to show quotation marks.

## Enclosing the blockquote tag in fancy looking quotes

- Open the `stylesheets/blockquote` folder.

- Create a file called `stylesheets/blockquote/_settings.scss`.

```
$quote-size: 200px;
```

- `blockquote` is in the _heaven_, so open `stylesheets/blockquote/_heaven.scss`.

- Replace with the following code:

```
@import "settings";

blockquote {
  quotes: "\201C""\201D""\2018""\2019";
  padding-top: $quote-size / 3 !important;
  padding-bottom: $quote-size / 1.6 !important;
  background: none;

  &:before {
    content: open-quote;
    color: $god-color;
    display: inline;
    font-size: $quote-size;
    height: 0;
    left: $personal-space * -1;
    line-height: 0;
    position: relative;
    top: $quote-size / 3;
  }

  &::after {
    content: close-quote;
    color: $god-color;
    display: inline;
    float: right;
    font-size: $quote-size;
    height: 0;
    line-height: 0;
    position: relative;
    right: $personal-space;
    top: $quote-size / 1.6;
  }
}
```

- Close and save `blockquote/_heaven.scss`.

- Close and save `blockquote/settings.scss`.

### What we learnt

1. The tagName must be the outer most tag in the file.

2. Creating a settings file special to this tag. `@import` as required. local settings just for this tagName.

3. Having tagName settings separately help keep **god**'s basic settings file short.
