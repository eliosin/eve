# Theme a nav in hell Quickstart

We can also theme tagNames in _hell_. Let's try a common usage and theme a fixed `nav` bar.

## Getting nav tags all sticky

### 1 Code a nav bar

First we will add a nav bar to the index.html file.

- Open the `index.html` file.

- Move the `nav` tag above the `h1` tag and replace its contents as shown:

```html
<nav>
  <a href="#h1" active>genesis</a>
  <a href="#h2">Yan</a>
  <a href="#h3">Sinean enkasike itak</a>
  <a href="#h4">Ronnu</a>
</nav>

<h1 id="h1">genesis</h1>
```

Add the target ids to the heading tags:

```html
<h2 id="h2">Yan</h2>

<h3 id="h3">Sinean enkasike itak</h3>

<article><img src="splash.jpg" /></article>

<h4 id="h4">Ronnu</h4>
```

- NB: No classNames. No unnessary nesting.

- Save and close the `index.html` file.

### 2 Style the nav bar

- Open the `stylesheets/nav` folder.

- `nav` is in the _hell_, so open `stylesheets/nav/_hell.scss`.

- Replace with the following code:

```scss
nav {
  @include colorize_god($god-blushed, $god-color);
  top: 0 !important;
  position: sticky;
  padding-bottom: $god-space !important;

  > a {
    @include colorize_god($god-blushed, $god-color);
    border-bottom: $god-color solid 0.5px;
    display: block;
    padding: $personal-space !important;
    text-decoration: none;
    &:last-child {
      border-bottom: none;
    }

    &:hover {
      @include colorize_god($hell-color, $god-blushed);
    }

    &[active] {
      @include colorize_god($god-color, $god-blushed);
    }
  }

  @media (max-width: $god-cosy) {
    top: 20 !important;
  }
}
```

### What we learnt

1. `position: sticky;` can be safely used for tagNames in _hell_ or _heaven_ because **god** takes care of their position.
