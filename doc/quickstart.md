# Quickstart eve

- [eve Prerequisites](/eliosin/eve/prerequisites.html)
- [Installing eve](/eliosin/eve/installing.html)

## Previously on eliosin...

- You completed **god** Quickstart [No!](/eliosin/god/quickstart.html)
- **eve** understood [No!](/eliosin/eve)
- [Check the Demo](/eliosin/eve/demo.html)
- **eliosin** prerequisites met? [No!](/eliosin/prerequisites.html)
- You completed **god** Quickstart [No!](/eliosin/god/quickstart.html)
- You created a new **eliosin** theme called `genesis`.
- You're currently running `gulp` in its root folder and can see the page.

## Nutshell

To add a new style for a tagName:

1. Create the file `stylesheets/<tagName>/_<prongName>.scss`

2. Make the tagName the parent tag of the scss, e.g.

```scss
tagName {
  // everything else in here
}
```

1. Reference the style's file in `stylesheets/theme.scss`

2. Add the tagName to the correct _prong_ in `stylesheets/settings.scss`

Do steps **1** to **4** in one go:

```
yo sin:eve
```

## Introduction

Take a look at your `stylesheets` folder. It should look something like this:

## Files Created/Affected

```
project
│   index.html # requires editing
└───stylesheets
    │   _heaven.scss
    │   _hell.scss
    │   _pillar.scss
    │   judge.scss
    │   settings.scss
    │   theme.scss
    └───aside
        │   _hell.scss
    └───...
        │   ....scss
    └───ul
        │   _pillar.scss
```

This structure is designed to help you organise and reuse - and for other eliosinners to share - **eliosin** themes and designs.

Given that **eliosin** works on tagNames, not classNames, each tagName has its own folder and some (or all) of the following files:

- `_pillar.scss` How the tagNamed will look in the _pillar_.
- `_heaven.scss` How the tagNamed will look in the _heaven_.
- `_hell.scss` How the tagNamed will look in the _hell_.
- `_theme.scss` How the tagNamed will look universally. Especially useful for inline tags not mentioned in the _container_.
- `_atheist.scss` How block tagNames will layout when not included in the _container_.
- `feature_name.scss` Add special cases or extras with a short descriptive name, like `accordian.scss`, `collapse-in-small-viewport.scss`.

A theme is fully 'fleshed' when its tagName folders contain all these files (and plenty of features). A fleshed theme will allow the user to put any tagName in _pillar_, _heaven_ or _hell_ with an expectation it will fit the page theme.

This tutorial will look at how this works in practice. Time to get your favourite text editor out!

```shell
cd ~/dev/folder/to/genesis
gulp
```

- Add `~/dev/folder/to/genesis` to your text editor's open projects.

## Exercise 1 Theme a tagName

- Open the `stylesheets/dl` folder.
- `dl` is in the _pillar_, so open `stylesheets/dl/pillar.scss`.
- Replace with the following code:

```sass
dl {
    align-items: stretch;
    display: flex;
    flex-flow: row wrap;

  dt {
    background: red;
    flex-basis: 100%;
    font-size: $god-spoke;
    font-weight: 400;
    padding: $personal-space;
  }

  dd {
    margin: 0;
    flex-basis: 100%;
    padding: $personal-space;
  }
}
```

- Save and close `dl/pillar.scss`.

- Look at the webpage.

Don't worry too much (for now) what the datalist looks like.

### What we learnt

1. The tagName must be the outer most tag in the class.

2. Using **god**'s default settings for common properties.

3. For instance, using `$god-blushed` so `dl`'s colors stay consistent with the whole theme.

4. Open the `_theme.scss` file. Notice how `dl/pillar` is already referenced? The yo generator did that for you.

**eliosin** was made for quickly creating wireframes. But that doesn't mean we can't give those elements a pretty face. Once tags are positioned the way you want, you can use the style-files to style them any way you want. Or don't want.

More examples here:

- [pillar table Quickstart](/eliosin/eve/quickstart-theming-table.html)
- [heaven blockquote Quickstart](/eliosin/eve/quickstart-theming-blockquote.html)
- [hell nav Quickstart](/eliosin/eve/quickstart-theming-nav.html)

## Exercise 2 Moving a Tag

We're going to move a tag from _hell_ to _heaven_.

### Add a style file

- Add a new file `stylesheets/h6/_hell.scss`.

- Edit the file - add the following:

```scss
h6 {
  @include colorize_god($god-blushed, $god-color);
}
```

- Close and save `stylesheets/h6/_hell.scss`.

### Reference the tag

- Back to your `stylesheets` folder.

- Open the `theme.scss` file.

- Change the reference from `h6/heaven` to `h6/hell`.

  ```
  @import 'h6/hell';
  ```

- Close and save `stylesheets/theme.scss`.

- Notice how this changes the color of your `h6` tag... but not its position on the page.

### Move the tag

- Back to your `stylesheets` folder.

- Open the `settings.scss` file.

- Move the `h6` tag into _hell_, as shown.

  ```
  $heaven: (blockquote, summary); // hangs right - gets the best views.
  $hell: (nav, aside, h6, details); // hangs left.
  ```

- Save and close the `stylesheets/settings.scss` file.

- Notice how the 'h6 in hell' tag now appears in the _hell_ side of the page.

### Exercise: details has repented

Your turn now! Try moving the `detail` tag into _heaven_. Here are the steps:

- Create the `detail/_heaven.scss` file.

- Open the `theme.scss` file, and change the reference from `detail/hell` to `detail/heaven`.

- Open the `settings.scss` file, and move `detail` into the `$heaven` list.

### What we learnt

1. How to use `settings.scss` to move tags into different prongs.

2. How to create a `tagName/_<prong>.scss` file for its style.

3. How to use `theme.scss` to change the style.

## Exercise 3 Theming a general tag

This explains how you style a tagName which is not mentioned in the _pillar_.

In this exercise we will style the `button` tag.

### Creating a style file

- Create the folder `stylesheets/button`.

- Create the file `stylesheets/button/_inline.scss`.

  - The file name `_inline.scss` makes it clear this will affect tagNames when they appear inside the prongs.
  - NB: It won't affect tagNames outside of the container: nothing in eliosin does!

- Save the file `stylesheets/button/_inline.scss` - but leave it open.

### Adding it to the theme

- Open the file `theme.scss`.

- Add `@import 'button/inline` to the bottom of the file.

```scss
#{$container} {
  @import "button/inline";
}
```

- Save and close `stylesheets/_inline.scss`.

### Theme the tag

- Reopen `stylesheets/button/_inline.scss`.

- Add this to the file:

```scss
button {
  @include colorize_god($god-blushed, $god-color);
  font-size: $god-spoke;
  font-weight: bold;
  margin-left: $personal-space;
  padding: 2px $god-space 1px;
  text-decoration: none;
  border: $god-blushed 1px solid;

  &:hover {
    @include colorize_god($god-color, $god-blushed);
  }
  &:disabled {
    @include colorize_god($god-color, $god-blushed);
  }
}
```

### What we learnt

1. How to style an inline tagName.

## Exercise 4 Using yo sin:eve

**elioGod**'s **yeoman** generator, **generator-sin** can also be used to create new style files.

- Call `yo sin:eve`.

```shell
yo sin:eve
```

At the prompt:

- Type the name of a tagName - use `article` in this example. Allow overwrite.

- Type the name of a position - use 'pillar' in this example. Allow overwrite.

Open `stylesheets/article/_pillar.scss` and add the following text.:

```scss
article {
  object-fit: fill;
  height: auto;
  // media widths
  @include tight {
    @include fixed_width($hell-width / 4 + $heaven-width / 4);
  }
  @include cosy {
    @include fixed_width($hell-width / 3 + $heaven-width / 3);
  }
  @include loose {
    @include fixed_width($hell-width / 2 + $heaven-width / 2);
  }
}
```

- Save and close `article/_pillar.scss`.

- Open `index.html` and add a `article` under the first `h3` tag.

```html
</h3>

<article><img src='splash.jpg' /></article>
```

### What we learnt

1. `yo sin:eve` will create or overwrite (after confirming) the file `stylesheets/tagName/_prong.scss`.

2. `yo sin:eve` adds `tagName,` to the `$prong: ()` in `stylesheets/settings.scss`.

3. `yo sin:eve` adds `@import 'tagName/_prong.scss';` to `stylesheets/theme.scss`.

4. Save and close `index.html`.

Check the changes.

## Recap

### `stylesheets/tagName/_pillar.scss`

Style `tagName` in _prong_ _pillar_.

### `stylesheets/tagName/_hell.scss`

Style `tagName` in _prong_ _hell_.

### `stylesheets/tagName/_heaven.scss`

Style `tagName` in _prong_ _heaven_.

### `stylesheets/tagName/_athiest.scss`

Style `tagName` generally as a block tag not contained by _prongs_.

### `stylesheets/tagName/_inline.scss`

Style `tagName` generally as an inline tag.

### `stylesheets/tagName/_theme.scss`

Style `tagName` generally.

### `stylesheets/tagName/feature.scss`

Style an optional, themic feature for `tagName`.

### `stylesheets/tagName/settings.scss`

`tagName` specific style settings.

## Tidy up

- Press `CTRL` and `C` to stop the browser.

- Type `npm run prettier` to make your work `prettier`.

- Type `npm run prettier`. You haven't written any tests yet, but there are 3 tests already which should always pass.

## Next

You have now seen how **eve** lets you theme tagNames for any of the _prongs_.

More examples here:

- [pillar table Quickstart](/eliosin/eve/quickstart-theming-table.html)
- [heaven blockquote Quickstart](/eliosin/eve/quickstart-theming-blockquote.html)
- [hell nav Quickstart](/eliosin/eve/quickstart-theming-nav.html)

Next you'll be using **adons**'s _patterns_ for changing themes based on human interaction.

- [adons Quickstart](/eliosin/adon/quickstart.html)
