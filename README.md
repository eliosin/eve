![](https://elioway.gitlab.io/eliosin/eve/elio-eve-logo.png)

> Be tempted **the elioWay**

# eve ![alpha](https://elioway.gitlab.io/eliosin/icon/devops/alpha/favicon.ico "alpha")

**eve** works with [god](https://elioway.gitlab.io/eliosin/god) as a jumpstarter when working with tagName-only CSS stylesheets.

- [eve Documentation](https://elioway.gitlab.io/eliosin/eve)
- [eve Demo](https://elioway.gitlab.io/eliosin/eve/demo.html)

## Installing

```shell
npm install @elioway/eve --save
yarn add  @elioway/eve --dev
```

- [Installing eve](https://elioway.gitlab.io/eliosin/eve/installing.html)

## Seeing is Believing

```shell
git clone https://gitlab.com/eliosin/eve/
cd eve
yarn
gulp
```

## Nutshell

### `gulp`

### `npm run test`

### `npm run prettier`

- [eve Quickstart](https://elioway.gitlab.io/eliosin/eve/quickstart.html)
- [eve Credits](https://elioway.gitlab.io/eliosin/eve/credits.html)

![](https://elioway.gitlab.io/eliosin/eve/apple-touch-icon.png)

## License

[HTML5 Boilerplate](LICENSE.txt) [Tim Bushell](mailto:theElioWay@gmail.com)
